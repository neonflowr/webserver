#include <winsock2.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <time.h>
#include <string.h>

#define PORT 8080
#define SERVER "127.0.0.1"

int main(int argc, char ** argv) {
  if (argc != 2) {
    printf("Not enough arguments\n");
    return 1;
  }
  char *msg = argv[1];
  printf("Message: %s\n", msg);

  WSADATA wsadata;
  if (WSAStartup(MAKEWORD(2, 2), &wsadata)) {
    printf("Winsock init failure\n");
    return 1;
  }

  int opt = 1;

  struct sockaddr_in serv_address;
  int serv_addr_len = sizeof(serv_address);
  serv_address.sin_family = AF_INET;
  serv_address.sin_port = htons(PORT);
  serv_address.sin_addr.S_un.S_addr = inet_addr(SERVER);

  int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  int timeout = 3000;

  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char *)&timeout, sizeof(timeout));

  char buffer[1024];
  int n;

  int msec;


  for(int i = 1; i <= 10; i++) {
    // start timer
    clock_t start = clock();

    memset(buffer, '\0', 1024);
    sendto(sock, msg, 1024, 0, (struct sockaddr *)&serv_address, serv_addr_len);

    printf("Message sent\n");

    n = recvfrom(sock, buffer, 1024, 0, (struct sockaddr *)&serv_address,
                 &serv_addr_len);
    buffer[n] = '\0';

    printf("Received: %s\n", buffer);
    printf("Len: %d\n", (int)strlen(buffer));

    if (strlen(buffer) == 0) {
      printf("Request timed out\n");
      continue;
    }

    clock_t diff = clock() - start;

    msec = diff * 1000 / CLOCKS_PER_SEC;
    printf("RTT %d: %d\n", i, msec / 1000);
  }

  closesocket(sock);

  return 0;
}
