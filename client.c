#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int main(int argc, char **argv) {
  if (argc != 5) {
    printf("Missing arguments\n");
    return 1;
  }

  int port = atoi(argv[2]);
  char *ip = argv[1];
  char *filename = argv[3];
  char *hostname = argv[4];

  printf("Starting\n");

  int sock = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_port = htons(port);

  // Convert from hostname text to binary form
  inet_pton(AF_INET, ip, &address.sin_addr);

  if (connect(sock, (struct sockaddr *)&address, sizeof(address)) != 0) {
    printf("Failed to connect to server\n");
    return 1;
  }

  printf("Connected\n");

  char *msg = malloc(sizeof(char) * 1024);
  snprintf(msg, 1024,
           "GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n",
           filename, hostname);

  printf("Request:\n%s", msg);

  if (send(sock, msg, strlen(msg), 0) == -1) {
    printf("Failed to send request\n");
    return 1;
  }

  free(msg);

  // get response
  char *res = malloc(sizeof(char) * 1024);
  printf("Response first 1024 bytes:\n");

  while (read(sock, res, 1024) > 0) {
    printf("%s", res);
  }

  printf("Finished\n");

  free(res);

  close(sock);

  return 0;
}
