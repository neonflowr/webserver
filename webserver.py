#import socket module
from socket import *

import sys # In order to terminate the program

serverSocket = socket(AF_INET, SOCK_STREAM)
#Prepare a sever socket



#Fill in start

serverPort = 12000
serverSocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
serverSocket.bind(("", serverPort))
serverSocket.listen(1)

#Fill in end

while True:
    #Establish the connection
    print('Ready to serve...')
    connectionSocket, addr = serverSocket.accept()
    try:
        message = connectionSocket.recvfrom(1024)[0].decode()
        print("message")
        print(message)
        filename = message.split()[1]
        f = open(filename[1:], "r")
        outputdata = f.readlines()
        f.close()

        #Send one HTTP header line into socket
        #Fill in start
        # connectionSocket.send(  ("HTTP/1.1 200 OK\r\nServer: me\r\nConnection: close\r\nContent-Length: {}\r\nContent-Type: text/plain\r\n".format(len(outputdata))).encode() )
        connectionSocket.send("HTTP/1.1 200 OK\r\n".encode())
        connectionSocket.send("\r\n".encode())
        #Fill in end
        #Send the content of the requested file to the client

        for i in range(0, len(outputdata)):
            connectionSocket.send(outputdata[i].encode())
            connectionSocket.send("\r\n".encode())

        connectionSocket.close()
    except IOError:
        #Send response message for file not found
        #Fill in start
        connectionSocket.send("HTTP/1.1 404 NOT FOUND\r\n\r\n".encode())
        #Fill in end

        filename = "404.html"
        f = open(filename, "r")
        outputdata = f.readlines()
        f.close()

        for i in range(0, len(outputdata)):
            connectionSocket.send(outputdata[i].encode())
            connectionSocket.send("\r\n".encode())

        #Close client socket
        #Fill in start
        connectionSocket.close()
        #Fill in end
        # serverSocket.close()

        # sys.exit()#Terminate the program after sending the corresponding data