#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <pthread.h>

#define PORT 8080
#define CONNECTION_COUNT 100

void handle_request(int *c_sock) {
  int socket = *c_sock;

  char buffer[1024];

  read(socket, buffer, 1024);

  printf("Handling request\n%s",  buffer);

  char *header_tokens = strtok(buffer, " ");
  header_tokens = strtok(NULL, " ");

  /* printf("Handling request  %s\n",  buffer); */
  /* printf("Header Token %s\n", header_tokens); */

  if (header_tokens == NULL || strcmp("/", header_tokens) == 0) {
    // redirect to index.html
    char msg[] =
        "HTTP/1.1 301 Moved Permanently\r\nLocation: /index.html\r\n\r\n\r\n";
    send(socket, msg, strlen(msg), 0);
    close(socket);
  } else {
    char *filename = calloc(sizeof(char), strlen(header_tokens) + 1);
    strncpy(filename, header_tokens + 1, strlen(header_tokens));

    /* printf("Final Token %s\n", filename); */

    FILE *fd;
    if ((fd = fopen(filename, "rb")) == NULL) {
      char msg[] = "HTTP/1.1 404 NOT FOUND\r\n\r\n";
      send(socket, msg, strlen(msg), 0);
      char html[] = "<h1>404 NOT FOUND</h1>\r\n";
      send(socket, html, strlen(html), 0);
      close(socket);
    } else {
      char msg[] = "HTTP/1.1 200 OK\r\n\r\n";
      send(socket, msg, strlen(msg), 0);

      /* char *line = malloc(sizeof(char) * 1024); */
      /* while (fgets(line, 1024, fd) != NULL) { */
      /*   if (line) { */
      /*     send(socket, line, strlen(line), 0); */
      /*   } */
      /* } */

      fseek(fd, 0, SEEK_END);
      int size = ftell(fd);
      /* char clength_msg[64]; */
      /* sprintf(clength_msg, "Content-Length: %d\r\n\r\n", size); */
      /* send(socket, clength_msg, strlen(clength_msg), 0); */
      fseek(fd, 0, SEEK_SET);

      char fbuf[size];

      while(!feof(fd)) {
        fread(fbuf, 1, sizeof(fbuf), fd);
        send(socket, fbuf, sizeof(fbuf), 0);
        bzero(fbuf, sizeof(fbuf));
      }
      fclose(fd);

      /* free(line); */

      close(socket);
    }
    free(filename);
  }
}

void init() {
  int sock = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in address;
  int addrlen = sizeof(address);

  int opt = 1;

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

  bind(sock, (const struct sockaddr *)&address, sizeof(address));

  listen(sock, CONNECTION_COUNT);

  printf("Server up at port %d\n", PORT);

  while (1) {
    printf("Receving new connection\n");
    int new_socket =
        accept(sock, (struct sockaddr *)&address, (socklen_t *)&addrlen);
    pthread_t req;
    pthread_create(&req, NULL, (void *)handle_request, &new_socket);
  }
}

int main() {
  init();
  return 0;
}
