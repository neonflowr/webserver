#include <winsock2.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define PORT 8080
#define SEED 1235

void upper(char s[], int n) {
	int mask = 0x20;
	int bit;
	for (int i = 0; s[i] != '\0'; i++) {
		if (s[i] >= 0x61 && s[i] <= 0x7A) {
			bit = (s[i] & mask) >> 5;
			if (bit) {
				s[i] = s[i] ^ ' ';
			}
		}
	}
}

int main(int argc, char** argv) {
	int iResult;

	WSADATA wsaData;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult) {
		printf("Failed to init winsock\n");
		return 1;
	}
	printf("Init sucess\n");

	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock == INVALID_SOCKET) {
		printf("Could not create socket\n");
		return 1;
	}
	printf("Socket created\n");

	struct sockaddr_in address;
	int addrlen = sizeof(address);

	const char opt = 1;

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

	if (bind(sock, (const struct sockaddr*)&address, sizeof(address)) != 0) {
		printf("Failed to bind\n");
		return 1;
	}
	printf("Bind success\n");

	srand(SEED);

	struct sockaddr_in from;
	int fromlen = sizeof(from);

	char message[1024];

	while (1) {
		printf("Receiving pings...\n");

		memset(message, '\0', 1024);

		// recv is for TCP
		int n;
		if ((n = recvfrom(sock, message, 1024, 0, (struct sockaddr*)&from, &fromlen)) == SOCKET_ERROR) {
			printf("recvfrom() failed\n");
			exit(1);
		}
		message[n] = EOF;

		printf("init message %s\n", message);

		upper(message, 1024);

		printf("upper message %s\n", message);

		if ((rand() % 100) < 40) {
			// simulate packet lost
			printf("Packet lost\n");
			continue;
		}

		if (sendto(sock, message, 1024, 0, (struct sockaddr*)&from, fromlen) == SOCKET_ERROR) {
			printf("sendto() failed\n");
			exit(1);
		}
	}

	closesocket(sock);
	WSACleanup();

	return 0;
}
