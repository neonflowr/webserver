#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define HOST "smtp.gmail.com"
#define PORT 587
#define BUF_SIZE 1024

int __substr(char *s, int n_s, char *p, int n_p) {
  int i = 0;
  while(s[i] != '\0') {
    int j = 0;
    while(j < n_p) {
      if (s[i] == p[j]) {
        i++;
        j++;
        if (j == n_p) {
          return 0;
        }
      } else {
        i++;
        break;
      }
    }
  }
  return -1;
}

int send_command(int sock, char msg[], char buf[], char reply_code[]) {
  printf("message %s\n", msg);

  if (send(sock, msg, strlen(msg), 0) == -1) {
    perror("Failed to send MAIL FROM\n");
    return -1;
  }

  bzero(buf, BUF_SIZE);
  int n;
  if (  (n = recv(sock, buf, BUF_SIZE, 0)) == -1 ) {
    perror("Failed to receive response\n");
    return -1;
  }
  buf[n] = '\0';

  printf("Response: %s\n", buf);
  if( __substr(buf, n, reply_code, 3) == -1) {
    printf("%s reply not received\n", reply_code);
    return -1;
  }
  printf("%s reply received\n", reply_code);

  return 0;
}

int main(int argc, char **argv) {
  int sock = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_port = htons(PORT);

  // resolve hostname
  struct hostent *h;
  struct in_addr **addr_list;
  if ((h = gethostbyname(HOST)) == NULL) {
    printf("Failed to resolve SMTP server hostname\n");
    return 1;
  }
  addr_list = (struct in_addr **)h->h_addr_list;

  for (int i = 0; addr_list[i] != NULL; i++) {
    printf("Host: %s\nAddress: %s\n", HOST, (char *)inet_ntoa(*(addr_list[i])));
  }

  inet_pton(AF_INET, (const char *)inet_ntoa(*addr_list[0]),
            &address.sin_addr.s_addr);

  if (connect(sock, (const struct sockaddr *)&address, sizeof(address)) != 0) {
    printf("Handshake failure\n");
    return 1;
  }

  printf("Connected\n");

  char buf[BUF_SIZE];
  bzero(buf, BUF_SIZE);

  {
    int n;
    if ((n = recv(sock, buf, BUF_SIZE, 0)) == -1) {
      perror("Failed to read server response\n");
      return 1;
    }
    buf[n] = '\0';
    printf("Response %s", buf);
    if (__substr(buf, n, "220", 3) == -1) {
      printf("220 reply not received\n");
      return 1;
    }
    printf("220 reply received\n");
  }

  // Send SMTP commands

  char msg[BUF_SIZE];

  // HELO
  bzero(msg, BUF_SIZE);
  strcpy(msg, "HELO Alice\r\n");
  send_command(sock, msg, buf, "250");

  // Start TLS session
  bzero(msg, BUF_SIZE);
  strcpy(msg, "STARTTLS\r\n");
  send_command(sock, msg, buf, "220");

  bzero(msg, BUF_SIZE);
  strcpy(msg, "HELO Alice\r\n");
  send_command(sock, msg, buf, "250");



  /* bzero(msg, BUF_SIZE); */
  /* strcpy(msg, "AUTH LOGIN\r\n"); */
  /* send_command(sock, msg, buf, "334"); */

  // MAIL FROM
  bzero(msg, BUF_SIZE);
  snprintf(msg, BUF_SIZE, "MAIL FROM: <%s>", "smtp@rpgradio.xyz");
  send_command(sock, msg, buf, "250");

  return 0;
}
