#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>

#define HOST "127.0.0.1"
#define PORT 8080
#define CONNECTION_COUNT 10
#define BUF_SIZE 550000

char *resolve_hostname(char *name)
{
    struct hostent *h;
    struct in_addr **addr_list;

    if ((h = gethostbyname(name)) == NULL)
    {
        return NULL;
    }

    addr_list = h->h_addr_list;

    return inet_ntoa(*addr_list[0]);
}

int send_request(int sock, char msg[], int n, char buf[], int n_buf)
{
    if (send(sock, msg, n, 0) == -1)
    {
        perror("Failed to send request\n");
        return -1;
    }

    int nread;
    if ((nread = recv(sock, buf, n_buf, 0)) == -1)
    {
        perror("Failed to read request response\n");
        return -1;
    }

    return 0;
}

int main(int argc, char **argv)
{
    // if (argc != 2) {
    //     printf("Usage: server hostname\nserver_ip: Hostname of the proxy location\n");
    //     return 1;
    // }

    int sock;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("Failed to create socket\n");
        exit(1);
    }

    struct sockaddr_in address;
    int addr_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(PORT);
    address.sin_addr.s_addr = inet_addr(HOST);

    {
        int optval = 1;
        setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    }

    if (bind(sock, &address, sizeof(address)) == -1)
    {
        perror("Failed to bind socket\n");
        exit(1);
    }

    if (listen(sock, CONNECTION_COUNT) == -1)
    {
        perror("Failed to start listening\n");
        exit(1);
    }

    char buf[BUF_SIZE];
    char msg[BUF_SIZE];
    char *hostname;
    char *header_tokens;
    char req[BUF_SIZE];
    char *res = calloc(sizeof(char), BUF_SIZE);
    // char path[BUF_SIZE];
    int n = 0;

    while (1)
    {
        int new_socket = accept(sock, &address, &addr_len);

        if (new_socket == -1)
        {
            perror("Error creating new socket for incoming connection\n");
            exit(1);
        }

        bzero(buf, BUF_SIZE);
        // bzero(path, BUF_SIZE);
        bzero(msg, BUF_SIZE);
        bzero(req, BUF_SIZE);
        bzero(res, BUF_SIZE);

        printf("Reading request\n");

        read(new_socket, buf, BUF_SIZE);

        printf("Request:\n%s\n", buf);

        strcpy(req, buf);

        header_tokens = strtok(buf, " ");
        header_tokens = strtok(NULL, " ");

        printf("Header token: %s\n", header_tokens);

        hostname = strtok(header_tokens, "/");
        hostname = strtok(NULL, "/");

        if (0)
        {
            printf("Something\n");
            exit(1);
        }
        else
        {
            char *ip;

            ip = resolve_hostname(hostname);
            if (ip == NULL)
            {
                printf("Failed to resolve hostname\n");
                exit(1);
            }

            printf("Host: %s\n", ip);

            // Create request on behalf of client
            {
                printf("Creating request on behalf of client\n");
                int port = 80;

                int csock = socket(AF_INET, SOCK_STREAM, 0);
                if (csock == -1) {
                    perror("Failed to create socket to proxy host\n");
                    exit(1);
                }

                struct sockaddr_in saddress;
                saddress.sin_family = AF_INET;
                saddress.sin_port = htons(port);

                // Convert from hostname text to binary form
                inet_pton(AF_INET, ip, &saddress.sin_addr);

                printf("Connecting to host %s\n", hostname);
                if (connect(csock, (struct sockaddr *)&saddress, sizeof(saddress)) == -1)
                {
                    perror("Failed to connect to proxy host\n");
                    exit(1);
                }

                printf("Connected to host %s\n", hostname);

                printf("Proxied Request:\n%s", req);

                if (send(csock, req, strlen(req), 0) == -1)
                {
                    perror("Failed to send request\n");
                    exit(1);
                }

                // get response
                printf("Response:\n");

                // Send back client response
                int nread = 0;
                int result;
                while (nread < BUF_SIZE) {
                    result = read(csock, res + nread, 1024);
                    // printf("nread %d\n", nread);
                    if (result == 0) {
                        // printf("Finished\n");
                        break;
                    }
                    if (result == -1) {
                        printf("Read failure\n");
                        exit(1);
                    }
                    nread += result;
                }

                printf("%s\n", res);

                close(csock);
                send(new_socket, res, strlen(res), 0);

                printf("Finished updated even\n");
            }
        }


        close(new_socket);
        // exit(0);
    }

    return 0;
}