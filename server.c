#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 8080
#define CONNECTION_COUNT 1

int main() {
  int sock = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in address;
  int addrlen = sizeof(address);

  /* char res[] = "HTTP/1.1 200 OK\r\n\r\n"; */

  int opt = 1;

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

  bind(sock, (const struct sockaddr *)&address, sizeof(address));

  listen(sock, CONNECTION_COUNT);

  printf("Server up at port %d\n", PORT);

  while (1) {
    printf("Receving new connection\n");
    int new_socket = accept(sock, (struct sockaddr *)&address, (socklen_t *)&addrlen);

    char buffer[1024];

    read(new_socket, buffer, 1024);

    char *header_tokens = strtok(buffer, " ");
    header_tokens = strtok(NULL, " ");
    printf("Header Token %s\n", header_tokens);

    char *filename = calloc(sizeof(char), strlen(header_tokens) + 1);
    strncpy(filename, header_tokens+1, strlen(header_tokens));

    printf("Final Token %s\n", filename);

    FILE *fd;
    if ((fd = fopen(filename, "rb")) == NULL) {
      char msg[] = "HTTP/1.1 404 NOT FOUND\r\n\r\n";
      send(new_socket, msg, strlen(msg), 0);
      char html[] = "<h1>404 NOT FOUND</h1>\r\n";
      send(new_socket, html, strlen(html), 0);
      close(new_socket);
    } else {
      char msg[] = "HTTP/1.1 200 OK\r\n\r\n";
      send(new_socket, msg, strlen(msg), 0);

      char *line = malloc(sizeof(char) * 1024);
      while (fgets(line, 1024, fd) != NULL) {
        if (line) {
          send(new_socket, line, strlen(line), 0);
        }
      }

      fclose(fd);
      free(line);

      /* send(new_socket, res, strlen(res), 0); */

      close(new_socket);
    }
    free(filename);
  }

  return 0;
}
